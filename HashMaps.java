import java.util.*;

public class HashMaps {

    public static void main(String[] args) {

        Map<Integer, String> hashMap = new HashMap<>();

        hashMap.put(1, "Chaanyah");
        hashMap.put(2, "Danny");
        hashMap.put(3, "Jarod");
        hashMap.put(4, "Tony");
        hashMap.put(5, "Marvin");

        Map<Integer, String> hashMap2 = new HashMap<>();

        hashMap2.putAll(hashMap);
        hashMap2.put(6,"Johnny");
        hashMap2.remove(3);

        System.out.println(hashMap2);
        System.out.println(hashMap);
        System.out.println(hashMap.containsKey(5));


        Map<String, Integer> names = new HashMap<>();

        names.put("Jane", 25);
        names.put("Jane", 30);
        System.out.println(names.size());

        
        String a = " ";

        for (int i = 1; i <= 100; i++) {
            a = a + "a";
            System.out.println("{" + i + ":" + a + "}");
        }


    }

}

